# Simple Form

The simple form module is designed to make it easy for website administrators to create and customize forms on their site. Site-builders can choose from a selection of simple form plugins and configure them to meet the specific needs of the site. Simple forms can be placed using the block system.

The module also allows developers to create their own custom form plugins to expand the available options. Currently, the module includes a simple search plugin, but more plugins will be added in the future.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/simple_form).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/simple_form).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Recommended modules

[Block Field](https://www.drupal.org/project/block_field): If you require the rendering of a block within the context of a field, utilize this module.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to Administration » Structure » Block layout
2. To add a Simple Form, simply click on the "Place block" option in the desired region.
3. Search for the "Simple Form" block
4. Select the plugin name and enter the configurations for the simple form plugin.
5. Click on Save.

## Maintainers

- Travis Tomka - [droath](https://www.drupal.org/u/droath)
