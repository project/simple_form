<?php

declare(strict_types=1);

namespace Drupal\simple_form\Plugin\SimpleForm;

use Drupal\Core\Form\FormStateInterface;

/**
 * Define the search simple form plugin.
 *
 * @SimpleForm(
 *   id = "simple_form_search",
 *   label = @Translation("Simple Search"),
 *   form_classname = "\Drupal\simple_form\Form\SearchSimpleForm"
 * )
 */
class SearchSimpleFormPlugin extends SimpleFormPluginBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'search_url' => NULL,
      'search_label' => NULL,
      'search_query_key' => NULL,
      'search_placeholder' => NULL,
      'search_button_label' => $this->t('Search'),
      'search_label_visibility' => 'invisible',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->getConfiguration();

    $form['search_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Search URL'),
      '#description' => $this->t('Input the search URL to redirect the query.
        <br><strong>Note:</strong> The URL must start with / or https://.'),
      '#default_value' => $configuration['search_url'],
      '#pattern' => '^(\/|https?:\/\/).*',
    ];
    $form['search_query_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Search Query Key'),
      '#description' => $this->t('Input the expected search query key.'),
      '#default_value' => $configuration['search_query_key'],
    ];
    $form['search_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Label'),
      '#description' => $this->t(
        'Input the search label to use for the keyword input.'
      ),
      '#default_value' => $configuration['search_label'],
    ];
    $form['search_label_visibility'] = [
      '#type' => 'select',
      '#title' => $this->t('Search Label Visibility'),
      '#description' => $this->t(
        'Select the search label visibility,'
      ),
      '#options' => [
        'after' => $this->t('After'),
        'before' => $this->t('Before'),
        'invisible' => $this->t('Invisible'),
      ],
      '#default_value' => $configuration['search_label_visibility'],
      '#states' => [
        'visible' => [
          ':input[name="field_block_reference[0][settings][simple_form_settings][search_label]"]' => ['!value' => ''],
        ],
      ],
    ];
    $form['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Placeholder'),
      '#description' => $this->t('Input the placeholder text for the search input.'),
      '#default_value' => $configuration['search_placeholder'],
    ];
    $form['search_button_label'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Search Button Label'),
      '#description' => $this->t('Input the button label for the search input'),
      '#default_value' => $configuration['search_button_label'],
    ];

    return $form;
  }

}
