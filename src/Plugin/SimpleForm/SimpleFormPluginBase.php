<?php

declare(strict_types=1);

namespace Drupal\simple_form\Plugin\SimpleForm;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\simple_form\Contract\SimpleFormPluginInterface;
use Drupal\simple_form\Trait\ConfigurationPluginFormTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the simple form plugin base.
 */
abstract class SimpleFormPluginBase extends PluginBase implements SimpleFormPluginInterface {

  use ConfigurationPluginFormTrait;

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * The class constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FormBuilderInterface $form_builder
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(BlockPluginInterface $block): array {
    $build_form = [];

    if ($classname = $this->getFormClassname()) {
      /* @phpstan-ignore-next-line */
      $build_form = $this->formBuilder->getForm(
        $classname,
        [
          'simple_form' => [
            'configuration' => $this->getConfiguration(),
          ],
        ]
      );
    }

    return $build_form;
  }

  /**
   * Get the form classname.
   *
   * @return string|null
   *   Get the form classname.
   */
  protected function getFormClassname(): ?string {
    return $this->pluginDefinition['form_classname'] ?? NULL;
  }

}
