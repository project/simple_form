<?php

declare(strict_types=1);

namespace Drupal\simple_form\Plugin\Block;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Utility\Error;
use Drupal\simple_form\Contract\SimpleFormPluginInterface;
use Drupal\simple_form\Contract\SimpleFormPluginManagerInterface;
use Drupal\simple_form\Trait\AjaxFormStateTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the simple form block plugin.
 *
 * @Block(
 *   id = "simple_form",
 *   admin_label = @Translation("Simple Form")
 * )
 */
class SimpleFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use AjaxFormStateTrait;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * @var \Drupal\simple_form\Contract\SimpleFormPluginManagerInterface
   */
  protected SimpleFormPluginManagerInterface $simpleFormManager;

  /**
   * The class contractor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\simple_form\Contract\SimpleFormPluginManagerInterface $simple_form_manager
   *   The simple form manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory,
    SimpleFormPluginManagerInterface $simple_form_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger_factory->get('simple_form');
    $this->simpleFormManager = $simple_form_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('plugin.manager.simple_form')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'content' => [
        'value' => NULL,
        'format' => filter_default_format(),
      ],
      'simple_form' => NULL,
      'simple_form_settings' => [],
      'theming' => [
        'suggestions' => NULL,
      ],
    ];
  }

  /**
   * The block process callback.
   *
   * @param array $form
   *   The form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   The processed form elements.
   */
  public function blockProcess(
    array $form,
    FormStateInterface $form_state
  ): array {
    $parents = $form['#parents'] ?? [];
    $wrapper_id = 'simple-form-block';

    $configuration = $this->getConfiguration();

    $form['#prefix'] = "<div id='$wrapper_id'>";
    $form['#suffix'] = '</div>';

    $simple_form_plugin_id = $this->getFormStateValue(
      [...$parents, 'simple_form'],
      $form_state,
      $this->getSimpleFormPluginId()
    );

    $form['simple_form'] = [
      '#type' => 'select',
      '#title' => $this->t('Simple Form'),
      '#required' => TRUE,
      '#options' => $this->simpleFormManager->getDefinitionOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $simple_form_plugin_id,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => $wrapper_id,
        'callback' => [$this, 'ajaxFormCallback'],
      ],
    ];

    if (!empty($simple_form_plugin_id)) {
      try {
        $instance = $this->simpleFormManager->createInstance(
          $simple_form_plugin_id,
          $form_state->getValue(
            [...$parents, 'simple_form_settings'],
            $this->getSimpleFormPluginSettings()
          )
        );

        if ($instance instanceof PluginFormInterface) {
          $subform = ['#parents' => [...$parents, 'simple_form_settings']];
          $configuration_form = $instance->buildConfigurationForm(
            $subform,
            SubformState::createForSubform(
              $subform,
              $form,
              $form_state
            )
          );

          if (count(Element::children($configuration_form)) !== 0) {
            $form['simple_form_settings'] = [
              '#type' => 'details',
              '#title' => $this->t('Simple Form Settings'),
              '#open' => TRUE,
            ] + $configuration_form;
          }
        }
      }
      catch (\Exception $exception) {
        Error::logException($this->logger, $exception);
      }
    }
    $form['content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Additional Content'),
      '#format' => $configuration['content']['format'],
      '#default_value' => $configuration['content']['value'],
    ];
    $form['theming'] = [
      '#type' => 'details',
      '#title' => $this->t('Theming'),
      '#open' => FALSE,
    ];
    $form['theming']['suggestions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Theme Suggestions'),
      '#description' => $this->t(
        'Input a theme suggestion.
         </br><strong>Note:</strong> Add each theme suggestion on a separate line.'
      ),
      '#default_value' => implode("\r\n", $this->getThemingSuggestions()),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ): array {
    $form['#process'][] = [$this, 'blockProcess'];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockValidate($form, FormStateInterface $form_state): void {
    if ($simple_form_plugin_id = $form_state->getValue('simple_form')) {
      try {
        $instance = $this->simpleFormManager->createInstance(
          $simple_form_plugin_id,
          $form_state->getValue('simple_form_settings', [])
        );

        if ($instance instanceof PluginFormInterface) {
          $subform = $form['simple_form_settings'] ?? [];
          $instance->validateConfigurationForm(
            $subform,
            SubformState::createForSubform(
              $subform,
              $form,
              $form_state
            )
          );
        }
      }
      catch (\Exception $exception) {
        Error::logException($this->logger, $exception);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    if ($simple_form_plugin_id = $form_state->getValue(['simple_form'])) {
      try {
        $instance = $this->simpleFormManager->createInstance(
          $simple_form_plugin_id,
          $form_state->getValue(['simple_form_settings'], [])
        );

        if (
          $instance instanceof PluginFormInterface
          && $instance instanceof ConfigurableInterface
        ) {
          $subform = $form['simple_form_settings'] ?? [];
          $instance->submitConfigurationForm(
            $subform,
            SubformState::createForSubform(
              $subform,
              $form,
              $form_state
            )
          );

          $form_state->setValue(
            ['simple_form_settings'],
            $instance->getConfiguration()
          );
        }
      }
      catch (\Exception $exception) {
        Error::logException($this->logger, $exception);
      }
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    foreach (array_keys($this->defaultConfiguration()) as $key) {
      $this->setConfigurationValue(
        $key,
        $form_state->getValue($key)
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $build = [
      '#block' => $this,
      '#theme' => 'simple_form',
    ];
    $configuration = $this->getConfiguration();

    if ($plugin = $this->getSimpleFormPlugin()) {
      $build += [
        '#form' => $plugin->buildForm($this),
      ];
    }

    if ($content_value = $configuration['content']['value'] ?? NULL) {
      $build['#content'] = check_markup(
        $content_value,
        $configuration['content']['format']
      );
    }

    return $build;
  }

  /**
   * Get the simple form plugin ID.
   *
   * @return string|null
   *   The simple form plugin ID.
   */
  public function getSimpleFormPluginId(): ?string {
    return $this->getConfiguration()['simple_form'];
  }

  /**
   * Get the simple form plugin theming suggestions.
   *
   * @return array
   *   An array of theming suggestions.
   */
  public function getThemingSuggestions(): array {
    if ($suggestions = $this->getConfiguration()['theming']['suggestions']) {
      return explode("\r\n", $suggestions);
    }
    return [];
  }

  /**
   * Get the simple form plugin instance.
   *
   * @return \Drupal\simple_form\Contract\SimpleFormPluginInterface|null
   *   The simple form plugin instance; otherwise NULL.
   */
  protected function getSimpleFormPlugin(): ?SimpleFormPluginInterface {
    try {
      /* @phpstan-ignore-next-line */
      return $this->simpleFormManager->createInstance(
        $this->getSimpleFormPluginId(),
        $this->getSimpleFormPluginSettings()
      );
    }
    catch (\Exception $exception) {
      Error::logException($this->logger, $exception);
    }

    return NULL;
  }

  /**
   * Get the simple form plugin settings.
   *
   * @return array
   *   An array of form plugin settings.
   */
  protected function getSimpleFormPluginSettings(): array {
    return $this->getConfiguration()['simple_form_settings'] ?? [];
  }

}
