<?php

declare(strict_types=1);

namespace Drupal\simple_form\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Define the simple form annotation.
 *
 * @Annotation
 */
class SimpleForm extends Plugin {

  /**
   * Simple form plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * Simple form plugin classname.
   *
   * @var string
   */
  public string $form_classname;

  /**
   * Simple form plugin label.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public TranslatableMarkup $label;

}
