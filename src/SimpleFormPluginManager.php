<?php

declare(strict_types=1);

namespace Drupal\simple_form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\simple_form\Annotation\SimpleForm;
use Drupal\simple_form\Contract\SimpleFormPluginInterface;
use Drupal\simple_form\Contract\SimpleFormPluginManagerInterface;

/**
 * Define the simple form plugin manager.
 */
class SimpleFormPluginManager extends DefaultPluginManager implements SimpleFormPluginManagerInterface {

  /**
   * The class constructor.
   *
   * @param \Traversable $namespaces
   *   The namespaces service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/SimpleForm',
      $namespaces,
      $module_handler,
      SimpleFormPluginInterface::class,
      SimpleForm::class,
    );
    $this->alterInfo('simple_form_info');
    $this->setCacheBackend($cache_backend, 'simple_form');
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
