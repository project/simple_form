<?php

declare(strict_types=1);

namespace Drupal\simple_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Define the search simple form class.
 */
class SearchSimpleForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'simple_form_search';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->resolveSimpleFormConfiguration($form_state);

    $form['keyword'] = [
      '#type' => 'textfield',
      '#placeholder' => $configuration['search_placeholder'] ?? NULL,
    ];

    if ($search_label = $configuration['search_label'] ?? NULL) {
      $visibility = $configuration['search_label_visibility'] ?? 'invisible';
      $form['keyword']['#title'] = $search_label;
      $form['keyword']['#title_display'] = $visibility;
    }

    $form['button'] = [
      '#type' => 'submit',
      '#value' => $configuration['search_button_label'] ?? $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $configuration = $this->resolveSimpleFormConfiguration($form_state);

    if (isset($configuration['search_url'], $configuration['search_query_key'])) {
      $keyword = $form_state->getValue('keyword');
      $form_state->setRedirectUrl(
        Url::fromUserInput($configuration['search_url'])->setOption('query', [
          $configuration['search_query_key'] => $keyword,
        ])
      );
    }
  }

  /**
   * Resolve the simple form configuration.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of simple form configurations.
   */
  protected function resolveSimpleFormConfiguration(
    FormStateInterface $form_state
  ): array {
    foreach ($form_state->getBuildInfo()['args'] ?? [] as $build_info) {
      if (!isset($build_info['simple_form'])) {
        continue;
      }
      return $build_info['simple_form']['configuration'] ?? [];
    }

    return [];
  }

}
