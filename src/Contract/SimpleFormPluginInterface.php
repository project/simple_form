<?php

declare(strict_types=1);

namespace Drupal\simple_form\Contract;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Define the simple form plugin interface.
 */
interface SimpleFormPluginInterface extends ContainerFactoryPluginInterface, ConfigurationPluginInterface {

  /**
   * Build the simple form.
   *
   * @return array
   *   The simple form render array.
   */
  public function buildForm(BlockPluginInterface $block): array;

}
