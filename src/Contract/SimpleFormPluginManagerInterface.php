<?php

declare(strict_types=1);

namespace Drupal\simple_form\Contract;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Define the simple form plugin manager interface.
 */
interface SimpleFormPluginManagerInterface extends PluginManagerInterface {

  /**
   * Get the plugin definition options.
   *
   * @return array
   *   An array of the plugin options.
   */
  public function getDefinitionOptions(): array;

}
