<?php

declare(strict_types=1);

namespace Drupal\simple_form\Contract;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Define the configuration plugin interface.
 */
interface ConfigurationPluginInterface extends PluginFormInterface, ConfigurableInterface {}
