<?php

declare(strict_types=1);

namespace Drupal\simple_form\Trait;

use Drupal\Component\Utility\NestedArray;

/**
 * Define the configurable plugin trait.
 */
trait ConfigurablePluginTrait {

  /**
   * Get the plugin default configuration.
   *
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * Gets this plugin's configuration.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration + $this->defaultConfiguration();
  }

  /**
   * Sets the configuration for this plugin instance.
   *
   * @param array $configuration
   *   An associative array containing the plugin's configuration.
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->getConfiguration(),
      $this->defaultConfiguration(),
      $configuration
    );
  }

}
